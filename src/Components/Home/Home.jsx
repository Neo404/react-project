import React, { Component } from 'react'
import './Home.css';
export class Home extends Component {
  state = {
    user_name: "",
     email : "",
     subject : "",
     message : ""
 
   };
   change  = e => {
     this.setState ({
       [e.target.name]: e.target.value
     });
   };
   onSubmit = e =>
   {
       e.preventDefault();
       console.log(this.state);
       this.setState ({
        user_name: "",
        email : "",
        subject : "",
        message : ""
     
       })
   };
  render() {
    return (
      <div>
          <div>
    {/*SLIDER HERE */}
      <div className="slider-custom" id="home">
        <div className="container">
          <div className="row">
            <div className="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div className="upper-header text-center center-block">
              <h1>we are creative agency</h1>
              <p>Morbi mattis felis at nunc. Duis viverra diam non justo. In nisl. Nullam sit amet magna in magna gravida vehicula. Mauris tincidunt sem sed arcu. Nunc posuere.</p>
              <button type="button" className="btn btn-light custom-btn-padd">Get Started</button>
              
              &nbsp;&nbsp;<button type="button" className="btn btn-primary custom-btn-padd">Learn More</button>
              </div>
            </div>
          </div>
        </div>

      </div>



    {/* END SLIDER HERE */}
    {/*welcome content */}
      <div className="welcome-content" id="about">
        <div className="container">
          <div className="row">
            <div className="col-lg-12 col-md-12 col-sm-12 col-xs-12">
              <h2 className="text-center">Welcome To Website</h2>
              <hr className="below-header-hr"></hr>
            </div>
          </div>
          <div className="row second-custom-row">
            <div className="col-lg-4 col-md-4 col-sm-12 col-xs-12">
              <div className="custom-box text-center center-block">
              <i className="fa fa-cogs fa-2x" aria-hidden="true"></i><br />
              <h4>Fully Customizable</h4>
              <p>Maecenas tempus tellus eget condimentum rhoncus sem quam semper libero sit amet.</p>
              <button className="btn btn-info custom-btn-welcome" type="button">Read More</button>
              </div>
            </div>

             <div className="col-lg-4 col-md-4 col-sm-12 col-xs-12">
              <div className="custom-box text-center center-block">
              <i className="fa fa-magic fa-2x" aria-hidden="true"></i><br />
              <h4>Awesome Features</h4>
              <p>Maecenas tempus tellus eget condimentum rhoncus sem quam semper libero sit amet.</p>
              <button className="btn btn-info custom-btn-welcome" type="button">Read More</button>
              </div>
            </div>

             <div className="col-lg-4 col-md-4 col-sm-12 col-xs-12">
              <div className="custom-box text-center center-block">
              <i className="fa fa-mobile fa-2x" aria-hidden="true"></i><br />
              <h4>Fully Responsive</h4>
              <p>Maecenas tempus tellus eget condimentum rhoncus sem quam semper libero sit amet.</p>
              <button className="btn btn-info custom-btn-welcome" type="button">Read More</button>
              </div>
            </div>
          </div>
        </div>
      </div>


    {/* ende welcome content */}
    {/* featured content */}
      <div className="featured-content" id="portfolio">
        <div className="container">
          <div className="row">
          <div className="col-lg-12 col-md-12 col-sm-12 col-xs-12">
              <h2 className="text-center">Featured Works</h2>
              <hr className="below-header-hr"></hr>
            </div>
          </div>

          <div className="row">
            <div className="col-lg-4 col-md-4 col-sm-12 col-xs-12">
              <div className="custom-images-featured">
                <img src = {require('../img/work1.jpg')} className="img-fluid image"/>
                <div className="overlay">
                  <div className="text">
                  <i className="fa fa-search" aria-hidden="true"></i>
                  </div>
                </div>
              </div>
            </div>

             <div className="col-lg-4 col-md-4 col-sm-12 col-xs-12">
              <div className="custom-images-featured">
                <img src = {require('../img/work6.jpg')} className="img-fluid image"/>
                <div className="overlay">
                  <div className="text">
                  <i className="fa fa-search" aria-hidden="true"></i>
                  </div>
              </div>
            </div>
            </div>

             <div className="col-lg-4 col-md-4 col-sm-12 col-xs-12">
              <div className="custom-images-featured">
                <img src = {require('../img/work2.jpg')} className="img-fluid image"/>
                <div className="overlay">
                  <div className="text">
                  <i className="fa fa-search" aria-hidden="true"></i>
                  </div>
              </div>
            </div>
              </div>

             <div className="col-lg-4 col-md-4 col-sm-12 col-xs-12">
              <div className="custom-images-featured">
                <img src = {require('../img/work3.jpg')} className="img-fluid image"/>
                <div className="overlay">
                  <div className="text">
                  <i className="fa fa-search" aria-hidden="true"></i>
                  </div>
              </div>
            </div>
            </div>

             <div className="col-lg-4 col-md-4 col-sm-12 col-xs-12">
              <div className="custom-images-featured">
                <img src = {require('../img/work4.jpg')} className="img-fluid image"/>
                <div className="overlay">
                  <div className="text">
                  <i className="fa fa-search" aria-hidden="true"></i>
                  </div>
              </div>
            </div>
            </div>

             <div className="col-lg-4 col-md-4 col-sm-12 col-xs-12">
              <div className="custom-images-featured">
                <img src = {require('../img/work5.jpg')} className="img-fluid image"/>
                <div className="overlay">
                  <div className="text">
                  <i className="fa fa-search" aria-hidden="true"></i>
                  </div>
              </div>
            </div>
          </div>
          </div>
                  </div>
      </div>


    {/*end featured content */}

    {/* offer section */}
        <div className = "offer-content" id="service">
          <div className="container">
            <div className="row">
            <div className="col-lg-12 col-md-12 col-sm-12 col-xs-12">
              <h2 className="text-center">What We Offer</h2>
              <hr className="below-header-hr"></hr>
            </div>

            </div>
              <div className="row">
                <div className="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                  <div className="custom-offer-box">
                  <div className="row">
                    <div className="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                    <i className="fa fa-diamond fa-2x" aria-hidden="true"></i>
                    </div>
                    <div className="col-lg-9 col-md-9 col-sm-12 col-xs-12 custom-parent-col-padding">
                      <h4>App Development</h4>
                      <p>Maecenas tempus tellus eget condimentum rhoncus sem quam semper libero.</p>
                    </div>
                    </div>
                  </div>
                </div>

                 <div className="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                  <div className="custom-offer-box">
                  <div className="row">
                    <div className="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                    <i className="fa fa-rocket fa-2x" aria-hidden="true"></i>
                    </div>
                    <div className="col-lg-9 col-md-9 col-sm-12 col-xs-12 custom-parent-col-padding">
                      <h4>Graphic design</h4>
                      <p>Maecenas tempus tellus eget condimentum rhoncus sem quam semper libero.</p>
                    </div>
                    </div>
                  </div>
                </div>

                 <div className="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                  <div className="custom-offer-box">
                  <div className="row">
                    <div className="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                    <i className="fa fa-cogs fa-2x" aria-hidden="true"></i>
                    </div>
                    <div className="col-lg-9 col-md-9 col-sm-12 col-xs-12 custom-parent-col-padding">
                      <h4>Creative Idea</h4>
                      <p>Maecenas tempus tellus eget condimentum rhoncus sem quam semper libero.</p>
                    </div>
                    </div>
                  </div>
                </div>

              <div className="row custom-featured-row">
              <div className="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                  <div className="custom-offer-box">
                  <div className="row">
                    <div className="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                    <i className="fa fa-diamond fa-2x" aria-hidden="true"></i>
                    </div>
                    <div className="col-lg-9 col-md-9 col-sm-12 col-xs-12 custom-parent-col-padding">
                      <h4>Marketing</h4>
                      <p>Maecenas tempus tellus eget condimentum rhoncus sem quam semper libero.</p>
                    </div>
                    </div>
                  </div>
                </div>

                 <div className="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                  <div className="custom-offer-box">
                  <div className="row">
                    <div className="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                    <i className="fa fa-pencil fa-2x" aria-hidden="true"></i>
                    </div>
                    <div className="col-lg-9 col-md-9 col-sm-12 col-xs-12 custom-parent-col-padding">
                      <h4>Awesome Support</h4>
                      <p>Maecenas tempus tellus eget condimentum rhoncus sem quam semper libero.</p>
                    </div>
                    </div>
                  </div>
                </div>

                 <div className="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                  <div className="custom-offer-box">
                  <div className="row">
                    <div className="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                    <i className="fa fa-flask fa-2x" aria-hidden="true"></i>
                    </div>
                    <div className="col-lg-9 col-md-9 col-sm-12 col-xs-12 custom-parent-col-padding">
                      <h4>Brand Design</h4>
                      <p>Maecenas tempus tellus eget condimentum rhoncus sem quam semper libero.</p>
                    </div>
                    </div>
                  </div>
                </div>
              </div>



              </div>





          </div>
        </div>



    {/* end offer section */}
    {/*project bg*/}
      <div className="project-bg">
        <div className="container">
          <div className="row">
            <div className="col-lg-3 col-md-3 col-sm-12 col-xs-12">
              <div className="clients-coutner text-center center-block">
              <i className="fa fa-users fa-2x" aria-hidden="true"></i>
              <h5>451</h5>
              <p>Happy Clients</p>
              </div>
            </div>
            <div className="col-lg-3 col-md-3 col-sm-12 col-xs-12">
              <div className="clients-coutner text-center center-block">
              <i className="fa fa-trophy fa-2x" aria-hidden="true"></i>
              <h5>12</h5>
              <p>Awards Won</p>
              </div>
            </div>
            <div className="col-lg-3 col-md-3 col-sm-12 col-xs-12">
              <div className="clients-coutner text-center center-block">
              <i className="fa fa-coffee fa-2x" aria-hidden="true"></i>
              <h5>154k</h5>
              <p>Cup of Coffee</p>
              </div>
            </div>
            <div className="col-lg-3 col-md-3 col-sm-12 col-xs-12">
              <div className="clients-coutner text-center center-block">
              <i className="fa fa-file fa-2x" aria-hidden="true"></i>
              <h5>45</h5>
              <p>Projects completed</p>
              </div>
            </div>
          </div>
        </div>
      
      </div>

    {/*end projects bg*/}
    {/*Pricing plan */}
      <div className="pricing-content" id="price">
        <div className="container">
          <div className="row">
          <div className="col-lg-12 col-md-12 col-sm-12 col-xs-12">
              <h2 className="text-center">Pricing Table</h2>
              <hr className="below-header-hr"></hr>
            </div>
          </div>

          <div className="row">
            <div className="col-lg-4 col-md-4 col-sm-12 col-xs-12">
             <div className="plan-custom-box text-center center-block">
              <h5>basic plan</h5>
              <div className="price-border">
              <h1>$19</h1>
              <h5><strong>/ month</strong></h5>
              </div>
              <p>1 GB Disk Space</p>
              <p>100 Email Account</p>
              <p>24 / 24 Support</p>
              <button type="button" className="btn btn-outline-primary">Purchase Now</button>
             </div> 
            </div>

            <div className="col-lg-4 col-md-4 col-sm-12 col-xs-12">
             <div className="plan-custom-box text-center center-block">
              <h5>silver plan</h5>
              <div className="price-border">
              <h1>$19</h1>
              <h5><strong>/ month</strong></h5>
              </div>
              <p>1 GB Disk Space</p>
              <p>100 Email Account</p>
              <p>24 / 24 Support</p>
              <button type="button" className="btn btn-outline-primary">Purchase Now</button>
             </div> 
            </div>

            <div className="col-lg-4 col-md-4 col-sm-12 col-xs-12">
             <div className="plan-custom-box text-center center-block">
              <h5>gold plan</h5>
              <div className="price-border">
              <h1>$19</h1>
              <h5><strong>/ month</strong></h5>
              </div>
              <p>1 GB Disk Space</p>
              <p>100 Email Account</p>
              <p>24 / 24 Support</p>
              <button type="button" className="btn btn-outline-primary">Purchase Now</button>
             </div> 
            </div>
          </div>
        </div>
      </div>


    {/*end pricing plan*/}
    {/*get in touch*/}
      <div className="contact-content" id="contact">
        <div className="container">
          <div className="row">
          <div className="col-lg-12 col-md-12 col-sm-12 col-xs-12">
              <h2 className="text-center">Get In Touch</h2>
              <hr className="below-header-hr"></hr>
            </div>
          </div>  
          <div className="row">
            <div className="col-lg-4 col-md-4 col-sm-12 col-xs-12">
              <div className="custom-contact-content-upper text-center">
              <i className="fa fa-phone fa-2x" aria-hidden="true"></i>
              <h4>Phone</h4>
              <p>512-423-7896</p>
              </div>
            </div>

            <div className="col-lg-4 col-md-4 col-sm-12 col-xs-12">
              <div className="custom-contact-content-upper text-center">
              <i className="fa fa-envelope fa-2x" aria-hidden="true"></i>
              <h4>Email</h4>
              <p>johndoe@gmail.com</p>
              </div>
            </div>

            <div className="col-lg-4 col-md-4 col-sm-12 col-xs-12">
              <div className="custom-contact-content-upper text-center">
              <i className="fa fa-map-marker fa-2x" aria-hidden="true"></i>
              <h4>Address</h4>
              <p>1739 - Bubby - Drive</p>
              </div>
            </div>
          </div>

          <div className="row">
            <div className="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <form>
  <div className="form-row">
    <div className="form-group col-md-6">
     
      <input type="text" className="form-control custom-input-field"  placeholder="Name" name="user_name" value={this.state.user_name} onChange={e => this.change(e)}/>
    </div>
    <div className="form-group col-md-6">
      
      <input type="email" className="form-control custom-input-field"  placeholder="Email" name="email" value={this.state.email} onChange={e => this.change(e)}/>
    </div>
  </div>
  <div className="form-group">
    
    <input type="text" className="form-control custom-input-field"  placeholder="Subject" name="subject" value={this.state.subject} onChange={e => this.change(e)}/>
  </div>

   <div className="form-group">
    
    <textarea className="form-control custom-input-field" placeholder="Message" name="message" value={this.state.message} onChange={e => this.change(e)}></textarea>
  </div>

      <div className="form-group text-center center-block">
  <button className="btn btn-primary custom-btn-welcome" onClick = {e => this.onSubmit(e)}>Send message</button>
  </div>
</form>
            </div>
          </div>




        </div>
      </div>
    {/*end get in touch */}
          
          </div>
        
      </div>
    )
  }
}
export default Home